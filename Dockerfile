FROM php:7.4-apache-buster AS build
ADD --chown=www-data . /src/mahara
RUN apt-get update; apt-get install -y libpq-dev \
    libpng-dev \
    libjpeg-dev \
    libfreetype6-dev \
    zlib1g-dev \
    libzip-dev \
    libicu-dev \
    git \
    unzip \
    npm; \
    apt-get clean
RUN npm install -g gulp
WORKDIR /src/mahara
RUN make css
RUN make cleanssphp && make ssphp
WORKDIR /src
RUN rmdir /var/www/html && ln -s /src/mahara/htdocs /var/www/html
WORKDIR /var/www/html
RUN docker-php-ext-configure gd --with-freetype --with-jpeg
RUN docker-php-ext-install pgsql gd zip intl
RUN yes '' | pecl install redis && docker-php-ext-enable redis
USER www-data
RUN cp config-environment.php config.php

FROM php:7.4-apache-buster
COPY --from=build /src /src
COPY --from=build /usr/local/lib/php /usr/local/lib/php
COPY --from=build /usr/local/etc/php /usr/local/etc/php
RUN rmdir /var/www/html && ln -s /src/mahara/htdocs /var/www/html
RUN apt-get update; apt-get install -y libpq5 \
    libpng16-16 \
    libjpeg62-turbo \
    libfreetype6 \
    libzip-dev \
    npm \
    sendmail; \
    apt clean
RUN npm install -g gulp
RUN mkdir /data && chown www-data:www-data /data
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"
RUN echo 'post_max_size = 100M' > "$PHP_INI_DIR/conf.d/post-max-size.ini"
VOLUME /data
EXPOSE 80
